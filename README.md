# Banking Demo

This is a sample application that demonstrates basic Account operations. At the time of writing it mostly just
 supports money transfer feature.


## Usage

### Building and Running

The demo Web app can be built through the Gradle wrapper included in this code base. The Java archive that includes
 all the dependencies required for execution is produced through the `fatJar` task:
 
     ./gradlew fatJar
     
or for short:

    ./gradle fJ
    
This task runs all the tests before the actual compilation and build.

The resulting Java archive can be run using the Java runtime,

    java -jar /build/libs/banking.jar
    
By default, the application will bind to port `9090`. This can be configured by setting the `PORT` environment variable,

    PORT=8088 java -jar /build/libs/banking.jar

### Demo

Included in this code base is a shell script that demonstrates the main money transfer feature. It only requires that
 the fat JAR is built in  `$PROJECT_ROOT/build/libs/banking.jar`. The demo script starts the application, sends a few
  requests through `curl`, then stops the application.
  
## REST API

The Banking Demo REST API current supports the following endpoints:

* POST `/accounts`

    Creates a new account with given starting balance.
    
    Sample payload:
    
      { "balance": "2500.25" }
      
     This returns details of the newly created account:
     
      {
         "number": 721,
         "balance": 2500.25
      }

* GET `/accounts/<number>`

    Retrieves the Account with the given number.
    
    For example:
    
      curl http://localhost:9090/accounts/721
      
    will return something like:
    
      {
          "number": 721,
          "balance": 2500.25
      }

* POST `/money/transfers`

    Creates a request to transfer money between given accounts.
    
    Sample payload:
    
      {
          "senderAccountNumber": 900,
          "recipientAccountNumber": 901,
          "amount": "5000"
      }
    
    When the operation succeeds, it responds with HTTP 204 (no content).
