#!/usr/bin/env bash

java -jar build/libs/banking.jar &

port=9090
[[ ! -z $PORT ]] && port=$PORT

curl http://localhost:$port/accounts &> /dev/null
while [ $? -ne 0 ]
do
  sleep 1
  curl http://localhost:$port/accounts &> /dev/null
done

echo $'\nCreating test Accounts...'
for amount in 777 33 10500; do
  curl -H "Content-type: application/json" \
  --data "{\"balance\": \"${amount}\"}" \
  http://localhost:$port/accounts

  echo
done

function transfer() {
  sender=$1
  recipient=$2
  amount=$3

  echo "Transferring $amount from account $sender to account $recipient..."
  curl -H 'Content-type: application/json'\
       -d "{\"senderAccountNumber\": $sender, \"recipientAccountNumber\": $recipient, \"amount\": "$amount"}"\
       http://localhost:$port/money/transfers
}

function show_accounts() {
  numbers="$@"
  for number in $numbers; do
    curl http://localhost:$port/accounts/$number
    echo
  done
}

# success
echo $'\nAccounts before transfer:'
show_accounts 1 2
echo
transfer 1 2 "12.50"
echo $'\nAccounts after transfer:'
show_accounts 1 2

# insufficient balance
echo $'\nAccounts before failed transfer:'
show_accounts 2 3
echo
transfer 2 3 "999.99"
echo $'\n\nAccounts after failed transfer:'
show_accounts 2 3

# negative amount
echo $'\nAccounts before failed transfer with negative amount:'
show_accounts 1 3
echo
transfer 3 1 "-200.00"
echo $'\n\nAccounts after failed transfer with negative amount:'
show_accounts 1 3

echo $'\nStopping the application...'
kill $(jps -l | grep banking.jar | awk '{print $1}')
echo 'Demo completed.'