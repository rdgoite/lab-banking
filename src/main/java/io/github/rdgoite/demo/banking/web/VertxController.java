package io.github.rdgoite.demo.banking.web;

import io.vertx.ext.web.Router;

public interface VertxController {

    void register(Router router);

}
