package io.github.rdgoite.demo.banking;

import io.github.rdgoite.demo.banking.exception.InsufficientBalance;
import io.github.rdgoite.demo.banking.exception.NegativeAmount;
import io.github.rdgoite.demo.banking.exception.NonExistentAccount;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Optional;

@Singleton
public class DefaultMoneyService implements MoneyService {

    private final AccountRepository accountRepository;

    @Inject
    public DefaultMoneyService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void fulfill(TransferRequest request) throws InsufficientBalance, NegativeAmount {
        Optional<Account> sender = accountRepository.findByNumber(request.getSenderAccountNumber());
        Optional<Account> recipient = accountRepository.findByNumber(request.getRecipientAccountNumber());
        if (sender.isPresent() && recipient.isPresent()) {
            sender.get().debit(request.getAmount());
            recipient.get().credit(request.getAmount());
        } else {
            //This is unchecked since Accounts can be checked before this method is called.
            throw new NonExistentAccount();
        }
    }

}
