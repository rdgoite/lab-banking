package io.github.rdgoite.demo.banking;

public interface AccountRegistrar {

    Account register(Account account);

}
