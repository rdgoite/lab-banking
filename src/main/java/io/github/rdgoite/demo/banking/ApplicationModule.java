package io.github.rdgoite.demo.banking;

import com.google.inject.AbstractModule;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

public class ApplicationModule extends AbstractModule {

    @Override
    protected void configure() {
        configureVertx();
        bind(AccountRegistrar.class).to(DefaultAccountRegistrar.class);
        bind(MoneyService.class).to(DefaultMoneyService.class);
        bind(AccountRepository.class).to(InMemoryAccountRepository.class);
    }

    private void configureVertx() {
        Vertx vertx = Vertx.vertx();
        bind(Vertx.class).toInstance(vertx);

        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        bind(Router.class).toInstance(router);
    }

}
