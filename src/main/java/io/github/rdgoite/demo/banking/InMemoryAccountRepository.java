package io.github.rdgoite.demo.banking;

import io.github.rdgoite.demo.banking.exception.DuplicateAccount;

import javax.inject.Singleton;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
public class InMemoryAccountRepository implements AccountRepository {

    private final Map<Integer, Account> accountStore = new ConcurrentHashMap<>();

    /**
     * <b>Important</b>: in-memory implementation of save does NOT support update-on-save. This will throw an exception
     * when saving an Account with a number that already exists on record.
     *<p>&nbsp;</p>
     * Due to the nature of Accounts stored in memory, changes to each instance are always committed on the spot and so
     * there's little use to do update-on-save. DuplicateAccount exception is implemented as a runtime exception so that
     * clients using AccountRepository interface are not forced to check for it as alternative, more persistent
     * implementations are expected to support update-on-save.
     */
    @Override
    public void save(Account account) {
        if (!accountStore.containsKey(account.getNumber())) {
            accountStore.put(account.getNumber(), account);
        } else {
            throw new DuplicateAccount();
        }
    }

    @Override
    public Optional<Account> findByNumber(int number) {
        return Optional.ofNullable(accountStore.get(number));
    }

    public void clear() {
        accountStore.clear();
    }

}
