package io.github.rdgoite.demo.banking;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class TransferRequest {


    private final int senderAccountNumber;
    private final int recipientAccountNumber;
    private final BigDecimal amount;

    @JsonCreator
    public TransferRequest(@JsonProperty("senderAccountNumber") int senderAccountNumber,
            @JsonProperty("recipientAccountNumber") int recipientAccountNumber,
            @JsonProperty("amount") BigDecimal amount) {
        this.senderAccountNumber = senderAccountNumber;
        this.recipientAccountNumber = recipientAccountNumber;
        this.amount = amount;
    }

    public int getSenderAccountNumber() {
        return senderAccountNumber;
    }

    public int getRecipientAccountNumber() {
        return recipientAccountNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

}
