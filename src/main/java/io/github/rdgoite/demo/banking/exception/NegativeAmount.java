package io.github.rdgoite.demo.banking.exception;

public class NegativeAmount extends Exception {

    public NegativeAmount() {
        super("Operation requires a non-negative amount.");
    }
}
