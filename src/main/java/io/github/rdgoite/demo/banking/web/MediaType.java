package io.github.rdgoite.demo.banking.web;

public enum  MediaType {

    APPLICATION_JSON("application/json");


    private final String mediaType;

    MediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String value() {
        return mediaType;
    }

    public String toString() {
        return value();
    }

}
