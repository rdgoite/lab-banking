package io.github.rdgoite.demo.banking;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.atomic.AtomicInteger;

@Singleton
public class DefaultAccountRegistrar implements AccountRegistrar {

    private final AtomicInteger accountNumber = new AtomicInteger(0);

    private final AccountRepository accountRepository;

    @Inject
    public DefaultAccountRegistrar(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Account register(Account account) {
        Account registeredAccount = new Account(accountNumber.incrementAndGet(), account.getBalance());
        accountRepository.save(registeredAccount);
        return registeredAccount;
    }

}
