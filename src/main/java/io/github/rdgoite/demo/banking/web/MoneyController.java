package io.github.rdgoite.demo.banking.web;

import io.github.rdgoite.demo.banking.MoneyService;
import io.github.rdgoite.demo.banking.TransferRequest;
import io.github.rdgoite.demo.banking.exception.InsufficientBalance;
import io.github.rdgoite.demo.banking.exception.NegativeAmount;
import io.github.rdgoite.demo.banking.exception.NonExistentAccount;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import javax.inject.Inject;
import java.util.Map;

import static io.github.rdgoite.demo.banking.web.StatusCode.*;

public class MoneyController implements VertxController {

    private final MoneyService moneyService;

    @Inject
    public MoneyController(MoneyService moneyService) {
        this.moneyService = moneyService;
    }

    @Override
    public void register(Router router) {
        router.post("/money/transfers")
                .consumes(MediaType.APPLICATION_JSON.value())
                .blockingHandler(this::transferMoney);
    }

    private void transferMoney(RoutingContext context) {
        TransferRequest transferRequest = context
                .getBodyAsJson()
                .mapTo(TransferRequest.class);
        try {
            moneyService.fulfill(transferRequest);
            context.response().setStatusCode(NO_CONTENT).end();
        } catch (InsufficientBalance insufficientBalance) {
            Map<String, String> details = Map.of("message", insufficientBalance.getMessage());
            context.response().setStatusCode(CONFLICT).end(Json.encode(details));
        } catch (NegativeAmount negativeAmount) {
            Map<String, String> details = Map.of("message", negativeAmount.getMessage());
            context.response().setStatusCode(BAD_REQUEST).end(Json.encode(details));
        } catch (NonExistentAccount nonExistentAccount) {
            Map<String, String> details = Map.of("message", nonExistentAccount.getMessage());
            context.response().setStatusCode(NOT_FOUND).end(Json.encode(details));
        }
    }

}
