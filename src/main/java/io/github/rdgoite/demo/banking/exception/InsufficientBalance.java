package io.github.rdgoite.demo.banking.exception;

public class InsufficientBalance extends Exception {

    public InsufficientBalance() {
        super("Failed to execute operation due to insufficient balance.");
    }

}
