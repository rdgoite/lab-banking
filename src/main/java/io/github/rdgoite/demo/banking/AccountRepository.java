package io.github.rdgoite.demo.banking;

import java.util.Optional;

public interface AccountRepository {

    void save(Account account);

    Optional<Account> findByNumber(int number);

}
