package io.github.rdgoite.demo.banking.web;

import io.github.rdgoite.demo.banking.Account;
import io.github.rdgoite.demo.banking.AccountRegistrar;
import io.github.rdgoite.demo.banking.AccountRepository;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import javax.inject.Inject;
import java.util.Optional;

import static com.google.common.net.MediaType.JSON_UTF_8;
import static io.github.rdgoite.demo.banking.web.MediaType.APPLICATION_JSON;
import static io.github.rdgoite.demo.banking.web.StatusCode.NOT_FOUND;
import static java.lang.Integer.parseInt;

public class AccountController implements VertxController {

    private final AccountRegistrar accountRegistrar;

    private final AccountRepository accountRepository;

    @Inject
    public AccountController(AccountRegistrar accountRegistrar, AccountRepository accountRepository) {
        this.accountRegistrar = accountRegistrar;
        this.accountRepository = accountRepository;
    }

    public void register(Router router) {
        router.post("/accounts")
                .consumes(APPLICATION_JSON.value())
                .produces(JSON_UTF_8.toString())
                .blockingHandler(this::createAccount);

        router.get("/accounts/:number")
                .produces(JSON_UTF_8.toString())
                .blockingHandler(this::getAccount);
    }

    private void createAccount(RoutingContext context) {
        Account account = context
                .getBodyAsJson()
                .mapTo(Account.class);
        Account savedAccount = accountRegistrar.register(account);
        context.response().end(Json.encode(savedAccount));
    }

    private void getAccount(RoutingContext context) {
        String number = context.request().getParam("number");
        Optional<Account> result = accountRepository.findByNumber(parseInt(number));
        result.ifPresentOrElse(account -> {
            context.response().end(Json.encode(account));
        }, () -> { context.response().setStatusCode(NOT_FOUND).end(); });
    }

}
