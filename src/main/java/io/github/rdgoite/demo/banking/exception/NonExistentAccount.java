package io.github.rdgoite.demo.banking.exception;

public class NonExistentAccount extends RuntimeException {

    public NonExistentAccount() {
        super("Account does not exist.");
    }

}
