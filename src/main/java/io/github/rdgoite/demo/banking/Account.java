package io.github.rdgoite.demo.banking;

import io.github.rdgoite.demo.banking.exception.InsufficientBalance;
import io.github.rdgoite.demo.banking.exception.NegativeAmount;

import java.math.BigDecimal;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Account implements Cloneable {

    private interface LockingOperation {

        void execute() throws Throwable;

    }

    private final Lock lock = new ReentrantLock();

    private Integer number;

    private BigDecimal balance;

    public Account() {
        this(null, BigDecimal.ZERO);
    }

    public Account(Integer number, BigDecimal balance) {
        this.number = number;
        this.balance = balance;
    }

    public Integer getNumber() {
        return number;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void debit(BigDecimal amount) throws InsufficientBalance, NegativeAmount {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new NegativeAmount();
        }

        if (this.balance.compareTo(amount) >= 0) {
            whileLocked(() -> {
                this.balance = this.balance.subtract(amount);
            });
        } else {
            throw new InsufficientBalance();
        }
    }

    public void credit(BigDecimal amount) throws NegativeAmount {
        if (amount.compareTo(BigDecimal.ZERO) >= 0) {
            whileLocked(() -> {
                this.balance = this.balance.add(amount);
            });
        } else {
            throw new NegativeAmount();
        }
    }

    private void whileLocked(LockingOperation operation) {
        lock.lock();
        try {
            operation.execute();
        } catch (Throwable cause) {
            throw new RuntimeException(cause);
        } finally {
            lock.unlock();
        }
    }

    public Account copy() {
        try {
            return (Account) clone();
        } catch (CloneNotSupportedException cause) {
            throw new RuntimeException(cause);
        }
    }

}
