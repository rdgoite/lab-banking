package io.github.rdgoite.demo.banking;

import com.google.inject.Guice;
import com.google.inject.Injector;
import io.github.rdgoite.demo.banking.web.AccountController;
import io.github.rdgoite.demo.banking.web.MoneyController;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;

import java.util.Optional;

public class Application {

    public static final String DEFAULT_PORT = "9090";

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new ApplicationModule());
        Router router = injector.getInstance(Router.class);

        setUpControllers(injector, router);

        Vertx vertx = injector.getInstance(Vertx.class);
        HttpServer server = vertx.createHttpServer();

        String port = Optional.ofNullable(System.getenv("PORT")).orElse(DEFAULT_PORT);
        System.out.printf("Runnning the application on port %s...\n", port);
        server.requestHandler(router).listen(Integer.parseInt(port));
    }

    private static void setUpControllers(Injector injector, Router router) {
        AccountController accountController = injector.getInstance(AccountController.class);
        accountController.register(router);

        MoneyController moneyController = injector.getInstance(MoneyController.class);
        moneyController.register(router);
    }

}
