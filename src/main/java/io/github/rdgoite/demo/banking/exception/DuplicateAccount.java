package io.github.rdgoite.demo.banking.exception;

public class DuplicateAccount extends RuntimeException {

    public DuplicateAccount() {
        this("Operation is not permitted due to Account duplication.");
    }

    public DuplicateAccount(String message) {
        super(message);
    }

}
