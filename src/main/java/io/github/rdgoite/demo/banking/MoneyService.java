package io.github.rdgoite.demo.banking;

import io.github.rdgoite.demo.banking.exception.InsufficientBalance;
import io.github.rdgoite.demo.banking.exception.NegativeAmount;

public interface MoneyService {

    void fulfill(TransferRequest transferRequest) throws InsufficientBalance, NegativeAmount;

}
