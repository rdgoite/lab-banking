package io.github.rdgoite.demo.banking.web;

public interface StatusCode {

    //ok
    int NO_CONTENT = 204;

    // error
    int BAD_REQUEST = 400;
    int NOT_FOUND = 404;
    int CONFLICT = 409;

}
