package io.github.rdgoite.demo.banking;

import io.github.rdgoite.demo.banking.exception.DuplicateAccount;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class InMemoryAccountRepositoryTest {

    private AccountRepository accountRepository = new InMemoryAccountRepository();

    @BeforeEach
    void setup() {
        ((InMemoryAccountRepository) accountRepository).clear();
    }

    @Test
    void createNewAccount() {
        //given:
        String initialBalance = "9371.20";
        Account account = new Account(77, new BigDecimal(initialBalance));

        //when:
        accountRepository.save(account);

        //then:
        Optional<Account> optionalAccount = accountRepository.findByNumber(77);
        assertThat(optionalAccount.isPresent()).isTrue();

        //and:
        Account savedAccount = optionalAccount.get();
        assertThat(savedAccount.getNumber()).isEqualTo(account.getNumber());
        assertThat(savedAccount.getBalance()).isEqualByComparingTo(initialBalance);
    }

    @Test
    void saveDuplicateAccount() {
        //given:
        Account originalAccount = new Account(331, new BigDecimal("990"));
        accountRepository.save(originalAccount);

        //and:
        Account duplicateAccount = originalAccount.copy();

        //expect:
        assertThatThrownBy(() -> {
            accountRepository.save(duplicateAccount);
        }).isInstanceOf(DuplicateAccount.class);
    }

    @Test
    void findNonExistentAccount() {
        //when:
        Optional<Account> account = accountRepository.findByNumber(99);

        //then:
        assertThat(account.isPresent()).isFalse();
    }

}
