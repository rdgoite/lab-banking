package io.github.rdgoite.demo.banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;
import static org.mockito.Mockito.*;

public class DefaultAccountRegistrarTest {

    @Nested
    @DisplayName("Registration")
    class AccountRegistration {

        private AccountRepository repository = mock(AccountRepository.class);
        private AccountRegistrar accountRegistrar = new DefaultAccountRegistrar(repository);

        @BeforeEach
        void setup() {
            reset(repository);
        }

        @Test
        @DisplayName("succeeds")
        void success() {
            //given:
            Account account = new Account();
            assumeThat(account.getNumber()).isNull();

            //when:
            Account registeredAccount = accountRegistrar.register(account);

            //then:
            assertThat(registeredAccount.getNumber()).isNotNull();
            assertThat(registeredAccount.getBalance()).isEqualByComparingTo(account.getBalance());
            verify(repository).save(registeredAccount);
        }

        @Test
        @DisplayName("generates unique account numbers")
        void uniqueNumbers() {
            //given: multiple accounts
            List<Account> testAccounts = IntStream
                    .range(0, 3)
                    .mapToObj(count -> new Account())
                    .collect(toList());

            //when: all accounts are registered
            List<Account> registeredAccounts = testAccounts.stream()
                    .map(accountRegistrar::register)
                    .collect(toList());

            //then: number set should contain all unique elements
            Set<Integer> accountNumbers = registeredAccounts.stream()
                    .map(Account::getNumber)
                    .collect(toSet());
            assertThat(accountNumbers).hasSameSizeAs(testAccounts);
        }

    }

}
