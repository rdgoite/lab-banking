package io.github.rdgoite.demo.banking;


import io.github.rdgoite.demo.banking.exception.InsufficientBalance;
import io.github.rdgoite.demo.banking.exception.NegativeAmount;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AccountTest {

    @Nested
    @DisplayName("Debit")
    class DebitTest {

        @Test
        @DisplayName("with sufficient balance")
        void sufficientBalance() throws Exception {
            //given:
            Account account = new Account(10, new BigDecimal("230.00"));

            //when:
            account.debit(new BigDecimal("21.75"));

            //then:
            assertThat(account.getBalance()).isEqualByComparingTo("208.25");
        }

        @Test
        @DisplayName("with insufficient balance")
        void insufficientBalance() {
            //given:
            Account account = new Account(233, new BigDecimal("132.43"));

            //expect:
            assertThatThrownBy(() -> {
                account.debit(new BigDecimal("200.00"));
            }).isInstanceOf(InsufficientBalance.class).hasNoCause();
        }

        @Test
        @DisplayName("with negative amount")
        void negativeAmount() {
            //given:
            Account account = new Account(6819, new BigDecimal("1901.43"));

            //expect:
            assertThatThrownBy(() -> {
                account.debit(new BigDecimal("-83.95"));
            }).isInstanceOf(NegativeAmount.class);
        }

    }

    @Nested
    @DisplayName("Credit")
    class CreditTest {

        @Test
        @DisplayName("valid amount")
        void validAmount() throws Exception {
            //given:
            Account account = new Account(21781, new BigDecimal("9310.25"));

            //when:
            account.credit(new BigDecimal("520.73"));

            //then:
            assertThat(account.getBalance()).isEqualByComparingTo("9830.98");
        }

        @Test
        @DisplayName("negative amount")
        void negativeAmount() {
            //given:
            Account account = new Account(90012, new BigDecimal("801.44"));

            //expect:
            assertThatThrownBy(() -> {
                account.credit(new BigDecimal("-921.99"));
            }).isInstanceOf(NegativeAmount.class);
        }

    }

}
