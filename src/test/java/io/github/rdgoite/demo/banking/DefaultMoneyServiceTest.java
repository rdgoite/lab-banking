package io.github.rdgoite.demo.banking;

import io.github.rdgoite.demo.banking.exception.InsufficientBalance;
import io.github.rdgoite.demo.banking.exception.NegativeAmount;
import io.github.rdgoite.demo.banking.exception.NonExistentAccount;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

public class DefaultMoneyServiceTest {

    @Nested
    @DisplayName("Money Transfer")
    class MoneyTransferTest {

        private AccountRepository accountRepository = mock(AccountRepository.class);
        private MoneyService moneyService = new DefaultMoneyService(accountRepository);

        @BeforeEach
        void setup() {
            reset(accountRepository);
        }

        @Test
        @DisplayName("succeeds")
        void transferSuccessfully() throws Exception {
            //given:
            Account sender = new Account(1, new BigDecimal("24790.81"));
            Account recipient = new Account(2, new BigDecimal("707.56"));

            //and:
            doReturn(Optional.of(sender)).when(accountRepository).findByNumber(1);
            doReturn(Optional.of(recipient)).when(accountRepository).findByNumber(2);

            //when:
            TransferRequest transferRequest = new TransferRequest(1, 2, new BigDecimal("100.10"));
            moneyService.fulfill(transferRequest);

            //then:
            assertThat(sender.getBalance()).isEqualByComparingTo("24690.71");
            assertThat(recipient.getBalance()).isEqualByComparingTo("807.66");
        }

        @Test
        @DisplayName("fails with insufficient balance")
        void transferWithInsufficientBalance() {
            //given:
            Account sender = new Account(77, new BigDecimal("10.59"));
            Account recipient = new Account(88, new BigDecimal("222.01"));

            //and:
            doReturn(Optional.of(sender)).when(accountRepository).findByNumber(77);
            doReturn(Optional.of(recipient)).when(accountRepository).findByNumber(88);

            //expect:
            assertThatThrownBy(() -> {
                TransferRequest transferRequest = new TransferRequest(77, 88, new BigDecimal("150.00"));
                moneyService.fulfill(transferRequest);
            }).isInstanceOf(InsufficientBalance.class);

            //and: ensure that recipient is not credited accidentally
            assertThat(recipient.getBalance()).isEqualByComparingTo("222.01");
        }

        @Test
        @DisplayName("fails when amount is negative")
        void transferNegativeAmount() {
            //given:
            Account sender = new Account(999, new BigDecimal("10289.99"));
            Account recipient = new Account(998, new BigDecimal("9231.00"));

            //and:
            doReturn(Optional.of(sender)).when(accountRepository).findByNumber(999);
            doReturn(Optional.of(recipient)).when(accountRepository).findByNumber(998);

            //expect:
            assertThatThrownBy(() -> {
                TransferRequest transferRequest = new TransferRequest(999, 998, new BigDecimal("-123"));
                moneyService.fulfill(transferRequest);
            }).isInstanceOf(NegativeAmount.class);
        }

        @Test
        @DisplayName("fails for non-existent Accounts")
        void transferForNonExistentAccounts() {
            //given:
            doReturn(Optional.ofNullable(null)).when(accountRepository).findByNumber(anyInt());

            //expect:
            assertThatThrownBy(() -> {
                TransferRequest request = new TransferRequest(1, 2, new BigDecimal("31.00"));
                moneyService.fulfill(request);
            }).isInstanceOf(NonExistentAccount.class);
        }

    }

}
